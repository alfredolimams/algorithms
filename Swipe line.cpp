#include <bits/stdc++.h>
using namespace std;
#define EPS 1e-9
#define INF 1<<20
#define pb push_back
#define DEG_to_RAD(x) (x/180.0)*M_PI
#define RAD_to_DEG(x) (x*180.0)/M_PI
#define DEBUG if(0)
#define MIN_to_DEG(x) x/60.0
#define in( l1, x, l2 ) l1 <= x && x <= l2 ? true : false
typedef vector<int> vi;
typedef pair<int,int> ii;
typedef vector<ii> vii;
typedef long long ll;

struct Point
{
public:
	double x, y;
	Point(){}
	Point(double x, double y){
		this->x = x;
		this->y = y; 
	}
	Point operator + ( Point p ){
		return Point( p.x + x, p.y + y );
	}
	Point operator - ( Point p ){
		return Point( x - p.x , y - p.y );
	}
	bool operator < ( Point p ) const {
		if ( x != p.x )
			return x < p.x;
		return y < p.y;
	}
	bool operator == ( Point p ) const {
		return x == p.x && y == p.y;
	}
};
typedef vector<Point> Points;
double dist( Point p , Point q ){
	return hypot( p.x - q.x, p.y - q.y );
}

struct Segment
{
	Point a, b;
	int id;
	Segment(){}
	Segment( Point s, Point e, int d ){
		a = s;
		b = e;
		id = d;
		if( b < a ){
			swap(a,b);
		}
	}
	bool operator < ( Segment s ) const {
		if( a == s.a ){
			return b < s.b;
		}
		return a < s.a;
	}
};
typedef set<Segment>::const_iterator ss_iter;
typedef vector<Segment> Segments;

double size( Segment s ){
	return hypot( s.a.x - s.b.x, s.a.y - s.b.y );
}

bool intersec( Segment s1, Segment s2 ){
	double dx1, dx2, dy1, dy2, det, u, t, dy, dx;
	dx1 = s1.b.x - s1.a.x;	dx2 = s2.b.x - s2.a.x;
	dy1 = s1.b.y - s1.a.y;	dy2 = s2.b.y - s2.a.y;
	dy = s2.a.y - s1.a.y; 	dx = s2.a.x - s1.a.x;
	det = dx2*dy1 - dy2*dx1;
	if( det == 0.0 )
		return false;
	u = ( dy * dx1 - dx * dy1 )/det;
	t = -( dx * dy2 - dy * dx2 )/det;
	return in( 0.0 , u , 1.0 ) && in( 0.0 , t , 1.0 );
}

int intersections( Segments &s ){
	sort( s.begin(), s.end() );
	set< Segment > active;
	ss_iter idx1, idx2;
	int ans = 0, tail = 0;
	for (int i = 0; i < s.size() ; ++i){
		
		Point p = Point( s[i].b.x - s[i].a.x , s[i].b.y - s[i].a.y );
		idx1 = lower_bound( active.begin(), active.end(), Segment( s[i].b - p , s[i].b - p , 0 ) );
		idx2 = upper_bound( active.begin(), active.end(), Segment( s[i].b + p , s[i].b + p , 0 ) );
		while( idx2 != idx1 ){
			if( intersec( s[i], *idx1 ) ){
				++ans;
			}
			idx1++;
		}
		active.insert( s[i] );
		while (s[i].a.x < s[tail].b.x)
		{
			active.erase(s[tail]);
			tail++;
		}
	}
	cout << ans << endl;
	return ans;
}

double closestpair( Points &p ){

	sort( p.begin(), p.end() );
	double h = INF;
	for (int i = 0; i < p.size() ; ++i){

		int idx;
		idx = upper_bound( p.begin(), p.end(), Point( p[i].x + h, p[i].y + h ) ) - p.begin();
		for (int j = i+1 ; j < idx ; ++j )
			h = min( h , dist( p[i], p[j] ) );
	}
	return h;
}

int main()
{
	int n;
	scanf("%d", &n);
	double x1, x2, y1, y2;
	Segments s;
	for (int i = 0; i < n ; ++i)
	{
		s.resize(n);
		scanf("%lf%lf%lf%lf", &x1, &y1, &x2, &y2);
		s[i] = Segment( Point(x1,y1), Point(x2,y2), i+1 );
	}
	intersections(s);
	vii ans;// = intersections(s);
	if( ans.size() ){
		printf("YES\n");
		for( auto p : ans )
			printf("%d %d\n", p.first+1, p.second+1 );
	}
	else{
		printf("NO\n");
	}
	return 0;
}