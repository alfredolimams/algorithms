#include <bits/stdc++.h>
using namespace std;
#define EPS 1e-9
#define INF 1<<20
#define pb push_back
#define DEG_to_RAD(x) (x/180.0)*M_PI
#define RAD_to_DEG(x) (x*180.0)/M_PI
#define DEBUG if(0)
#define MIN_to_DEG(x) x/60.0
#define in( l1, x, l2 ) l1-EPS <= x && x <= l2+EPS ? true : false
#define EQUAL( x, y ) ( fabs(x-y) < EPS ) ? true : false
typedef vector<int> vi;
typedef pair<int,int> ii;
typedef vector<ii> vii;
typedef long long ll;
typedef vector<vi> vvi;

vvi Adj[2];
vi vstd;

unordered_set<int> bfs( int u , int size , int gear)
{
	vi vst(size,0);
	vst[u] = 1;
	unordered_set<int> ans;
	ans.insert(u);
	queue<int> q; q.push(u);
	while( !q.empty() )
	{
		int v = q.front(); q.pop();
		for ( auto p : Adj[gear][v] )
				if( !vst[p] )
				{
					q.push(p);
					ans.insert(p);
					vst[p] = 1;
				}
	}
	return ans;
}

int strongly_connected( int size, int v )
{
	unordered_set<int> f1 = bfs(v,size,0);
	unordered_set<int> f2 = bfs(v,size,1);
	vi cfc;
	if( f1.size() < f2.size() )
	{
		for ( auto p : f1 )
			if( f2.count(p) )
				cfc.pb(p);
	}
	else
	{
		for ( auto p : f2 )
			if( f1.count(p) )
				cfc.pb(p);
	}
	if( cfc.size() == size )
		return 1;
	return 0;
}

int main(int argc, char const *argv[])
{
	int vertex, edges, type;
	while( cin >> vertex >> edges , vertex + edges )
	{
		Adj[0].resize(vertex);
		Adj[1].resize(vertex);
		for (int i = 0; i < edges ; ++i)
		{
			int f , a;
			cin >> f >> a >> type ;
			Adj[0][f-1].pb( a-1 );
			Adj[1][a-1].pb( f-1 );
			if( type == 2 )
			{
				Adj[1][f-1].pb( a-1 );
				Adj[0][a-1].pb( f-1 );
			}
		}
		cout << strongly_connected(vertex,0) << endl; 
	}

	return 0;
}