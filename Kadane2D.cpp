#include <bits/stdc++.h>
using namespace std;
#define MAX 20110
#define DEBUG if(0)
int M[MAX][MAX];
int value(int i, int j)
{
	int val = M[i][j];
	if( i ) val -= M[i-1][j];
	if( j ) val -= M[i][j-1];
	if( i && j ) val += M[i-1][j-1];
	return val;
}
int main()
{
	int maxSub, submax;
	int line, collumn, TC;
	cin >> TC;
	while(TC--)		///   	Size of Matrix
	{ 			
		cin >> line >> collumn >> k;
		for (int i = 0; i < line; ++i)
		{
			for (int j = 0; j < collumn; ++j)
			{
				cin >> M[i][j];
				if( i ) M[i][j] += M[i-1][j];
				if( j ) M[i][j] += M[i][j-1];
				if( i && j ) M[i][j] -= M[i-1][j-1];
			}
		}
		maxSub = 0;
		max_area = 0;
		for (int subline = 1; subline <= line; ++subline) 
			for (int subcollumn = 1; subcollumn <= collumn; ++subcollumn )     /// All submatrix size
			{
				for (int i = subline-1; i < line ; ++i)
				{
					for (int j = subcollumn-1; j < collumn ; ++j )
					{
						area = subline*subcollumn;
						submax = M[i][j];
						if( i >= subline )	submax -= M[i-subline][j];
						if( j >= subcollumn ) submax -= M[i][j-subcollumn];
						if( i >= subline && j >= subcollumn ) submax += M[i-subline][j-subcollumn];
						maxSub = max(maxSub,submax);
					}
				}
			}
		cout << maxSub << endl;
	}
	return 0;
}
