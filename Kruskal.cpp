#include <bits/stdc++.h>
using namespace std;
typedef vector<int> vi;
typedef pair<int,int> ii;
typedef vector<ii> vii;
typedef vector<vii> vvii;
typedef tuple<int,int,int> iii;
#define pb push_back
#define fi first
#define se second 
vvii graph;
vi parent;
vector<iii> edges;
void init(int vertex)
{
	//graph.clear();
	//graph.resize(vertex);
	edges.clear();
	parent.clear();
	parent.resize(vertex+1,-1);
}
int root( int x )
{
	while( parent[x] != x )
	{
		x = parent[x];
	}
	return x;
}

void union_root(int x, int y)
{
	parent[ root(x) ] = root(y);
}

int kruskal()
{
	int mst = 0;
	iii ed;
	int f, a, w;
	sort(edges.begin(), edges.end());
	for( auto p : edges )
	{
		w = get<0>(p);
		f = get<1>(p);
		a = get<2>(p);
		if( parent[f] == -1 ) parent[f] = f;
		if( parent[a] == -1 ) parent[a] = a;
		if( root(f) != root(a) )
		{
			mst += w;
			union_root(f,a);
		}
	}
	return mst;
}



int main()
{
	int vertex, edge;
	int from, arrival, weight;
	while( scanf("%d%d", &vertex, &edge) != EOF )
	{
		init(vertex);
		for (int i = 0; i < edge; ++i)
		{
			scanf("%d%d%d", &from, &arrival, &weight);
			edges.pb( iii(weight, from, arrival) );
		}
		printf("%d\n", kruskal());
	}
	return 0;
}