// Vampiros - Cadeia de Marcov

#include <bits/stdc++.h>
using namespace std;

int vp1, vp2, at, d;
double T[30][30];
double R[30][30];

double cal()
{
   memset( T, 0, sizeof T );
   T[0][0] = T[vp1+vp2][vp1+vp2] = 1;
   // Construção da matriz
   for(int i = 1 ; i < vp1+vp2 ; i++)
   {
      if( i+d < vp1+vp2 )
         T[i][i+d] = at/6.0;
      if( i+d >= vp1+vp2 )
         T[i][vp1+vp2] = at/6.0;
      if( i-d > 0)
         T[i][i-d] = (6-at)/6.0;
      if( i-d <= 0)
         T[i][0] = (6-at)/6.0;
   }
   // Exponiaciação de matrizes
   for(int l = 0 ; l < 10 ; ++l )
   {
      for(int i = 0 ; i <= vp1+vp2 ; i++ )
         for(int j = 0 ; j <= vp1+vp2 ; j++ )
         {
            double aux = 0;
            for(int k = 0; k <= vp1+vp2 ; k++ )
               aux += T[i][k] * T[k][j];
            R[i][j] = aux;
         }
      memcpy( T, R, sizeof R );
   }
   return R[vp1][vp1+vp2];
}

int main()
{
   while( scanf("%d%d%d%d",&vp1,&vp2,&at,&d), d )
   {
      printf("%.1lf\n", 100*cal() );
   }
   return 0;
}
