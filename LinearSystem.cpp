#include <bits/stdc++.h>
using namespace std;
#define DEBUG if(1)

/*	Solve of system linear Cramer's Lies
	|	A1	A2	A3	...	AN	|		|	X1	|		|	R1	|
	|	B1	B2	B3	...	BN	|	*	|	X2	|	=	|	R2	|
   ...					   ...	   ...			   ...
	|	N1	N2	N3	...	NN	|		|	XN	|		|	RN 	|

	O ( N³ )

*/

void init( vector< vector<double> > &matrix ,int n )
{
	matrix.clear();
	matrix.resize( n , vector<double>(n+1) );
}

double det( vector< vector<double> > &matrix )
{
	double result_main = 0, result_second = 0;
	int size = matrix.size();
	if( size == 1 ){
		return matrix[0][0];
	}
	else if( size == 2 ){
		return matrix[0][0]*matrix[1][1] - matrix[0][1]*matrix[1][0];
	}
	for (int i = 0; i < size ; ++i)
	{
		double lp = 1, ls = 1;
		for (int j = 0; j < size ; ++j)
		{
			lp *= matrix[j][(j+i)%size];
			ls *= matrix[j][((i+size-1-j)%size+size)%size];
		}
		result_main += lp;
		result_second += ls; 
	}
	return result_main - result_second;
}

vector<double> solve( vector< vector<double> > &matrix )
{
	int d = det(matrix);
	vector<double> answer;
	if( d == 0.0 ){
		cout << "Has infinite solutions" << endl;
		return answer;
	}
	int size = matrix.size();
	for (int i = 0; i < size ; ++i)
	{
		vector< vector<double > >matrix2 = matrix;
		for( int j = 0 ; j < size ; ++j )
			matrix2[j][i] = matrix[j][size];
		answer.push_back( det(matrix2)/d );
	}
	return answer;
}

int main(int argc, char const *argv[])
{
	vector< vector<double> > matrix;
	int n;
	cin >> n;
	init(matrix,n);
	for (int i = 0; i < n ; ++i)
	{
		for (int j = 0; j <= n ; ++j)
		{
			cin >> matrix[i][j];
		}
	}
	vector<double> ans = solve(matrix);
	for (int i = 0; i < ans.size() ; ++i)
		cout << "R"<<i+1 <<" = " << ans[i] << endl;

	return 0;
}