#include <bits/stdc++.h>
using namespace std;
#define EPS 1e-9
#define INF 1<<20
#define pb push_back
#define DEG_to_RAD(x) (x/180.0)*M_PI
#define RAD_to_DEG(x) (x*180.0)/M_PI
#define DEBUG if(0)
#define MIN_to_DEG(x) x/60.0
typedef vector<int> vi;
typedef long long ll;

struct Point
{
public:
	double x, y;
	Point();
	Point(double x, double y){
		this->x = x;
		this->y = y; 
	}
	Point operator + ( Point p ){
		return Point( p.x + x, p.y + y );
	}
	bool operator < ( Point p ) const {
		if ( x != p.x )
			return x < p.x;
		return y < p.y;
	}
};

double dist( Point p , Point q ){
	return hypot( p.x - q.x, p.y - q.y );
}

typedef vector<Point> Points;

double closestpair( Points &p ){

	sort( p.begin(), p.end() );
	double h = INF;
	for (int i = 0; i < p.size() ; ++i){

		int idx;
		idx = upper_bound( p.begin()+i , p.end(), Point( p[i].x + h, p[i].y + h ) ) - p.begin();
		for (int j = i+1 ; j < idx ; ++j )
			h = min( h , dist( p[i], p[j] ) );
	}
	return h;
}

int main()
{
	int n;
	double x, y;
	while( scanf("%d", &n), n ){
		Points p;
		for (int i = 0; i < n; ++i)
		{
			scanf("%lf%lf", &x, &y );
			p.pb(Point(x,y));
		}
		double h = closestpair(p);
		if( h < 10000.0 )
			printf("%.4lf\n", h );
		else
			printf("INFINITY\n");
	}
	return 0;
}