#include <bits/stdc++.h>
using namespace std;
#define EPS 1e-9
#define INF 1<<20
#define inf 0x3f3f3f3f
#define pb push_back
#define DEG_to_RAD(x) (x/180.0)*M_PI
#define RAD_to_DEG(x) (x*180.0)/M_PI
#define fi first
#define se second
#define DEBUG if(0)
#define MIN_to_DEG(x) x/60.0
#define in( l1, x, l2 ) l1 <= x && x <= l2 ? true : false
#define EQUAL(a,b) fabs(a-b) < EPS
#define MAX 100
typedef vector<int> vi;
typedef pair<int,int> ii;
typedef vector<ii> vii;
typedef long long ll;
typedef vector<vi> vvi;
typedef vector<vii> vvii;

vvi AdjMat;

void Warshall()
{
	/*
		O ( V ³ )
	*/
	for (int k = 0; k < V; k++)
		for (int i = 0; i < V; i++)
			for (int j = 0; j < V; j++)
				AdjMat[i][j] |= (AdjMat[i][k] & AdjMat[k][j]);
}