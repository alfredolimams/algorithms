#include <bits/stdc++.h>
using namespace std;
#define EPS 1e-9
#define INF 1<<20
#define inf 0x3f3f3f3f
#define pb push_back
#define DEG_to_RAD(x) (x/180.0)*M_PI
#define RAD_to_DEG(x) (x*180.0)/M_PI
#define fi first
#define se second
#define DEBUG if(0)
#define MIN_to_DEG(x) x/60.0
#define in( l1, x, l2 ) l1 <= x && x <= l2 ? true : false
#define EQUAL(a,b) fabs(a-b) < EPS
#define MAX 100
typedef vector<int> vi;
typedef pair<int,int> ii;
typedef vector<ii> vii;
typedef long long ll;
typedef vector<vi> vvi;
typedef vector<vii> vvii;

vvii AdjList;

bool cycle_negative( int v , int f )
{
	/*
		O ( V² * E )
	*/
	vi dist;
	dist.resize( v, inf );
	dist[f] = 0;
	for (int i = 0; i < v-1 ; ++i)
		for (int j = 0; j < v ; ++j)
			for( auto p : AdjList[j] )
				dist[ p.fi ] = min( dist[ p.fi ] , dist[j] + p.se );
	/*
		O ( V * E )
	*/	
	for (int j = 0; j < v ; ++j)
		for( auto p : AdjList[j] )
			if( dist[ p.fi ] > dist[j] + p.se )
				return true;
	return false;
}

int main(int argc, char const *argv[])
{
	return 0;
}