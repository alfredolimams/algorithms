#include <bits/stdc++.h>
using namespace std;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<ii> vii;
typedef vector<vii> vvii;
typedef long long ll;
#define pb push_back
#define INF 0xf3f3f3
#define DEBUG if(0)

struct trie
{
	int qt;
	bool leaf;
	map<int, trie*>childs;
	map<int,int> qt_childs;
	trie()
	{
		qt = leaf = 0;
	}
};

void remove(trie* root) 
{
	for (int i = 0; i < 26 ; ++i)
	{
    	if (!root->childs.count(i)) continue;
    	remove(root->childs[i]);
	}
    delete root;
}

void add( string p , int idx , trie * t )
{
	int value = tolower(p[idx]) - 'a';
	if( idx == p.size() )
	{
		t->leaf = true;
		return;
	}
	if( !t->childs.count(value) )
		t->childs[value] = new trie();
	++t->qt;
	++t->qt_childs[value];
	add( p, idx+1 , t->childs[value] );
}

int query( string p , int idx , trie *t)
{
	if( p.size() == idx )
	{
		return 0;
	}
	else
	{
		int value = tolower(p[idx]) - 'a';
		if( !t->childs.count(value) )
		{
			DEBUG cout << "Don't have" << endl;
			return 0;
		}
		DEBUG cout << t->qt << " de palavras" << endl;
		DEBUG cout << "Com " << p[idx] << " temos " << t->qt_childs[value] << endl;
		if( t->qt != t->qt_childs[value] || t->leaf )
			return 1 + query( p , idx+1 , t->childs[value] );
		else
			return query( p , idx+1 , t->childs[value] );
	}
}

int main(int argc, char const *argv[])
{
	int input;
	while( cin >> input )
	{
		vector<string> vs;
		vs.resize(input);
		for (int i = 0; i < input; ++i)
			cin >> vs[i];
		trie * t = new trie();
		for (int i = 0; i < input; ++i)
		{
			add( vs[i], 0 , t );
		}
		double sum = 0;
		for (int i = 0; i < input ; ++i)
		{
			int ans = 1 + query( vs[i] , 1 , t->childs[ vs[i][0]-'a' ] );
			sum += ans;
		}
		printf("%.2lf\n", (double)(sum)/input );
		remove(t);
	}
	return 0;
}