#include <bits/stdc++.h>
using namespace std;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<ii> vii;
typedef vector<vii> vvii;
typedef long long ll;
typedef pair<ll,ll> llll;

#define pb push_back
#define fi first
#define se second
#define DEBUG if(1)
#define VISITED 2
#define EXPLORED 1
#define UNVISITED 0
#define pb push_back
#define EPS 1e-9
#define INF 1<<20
#define inf 0x3f3f3f3f
#define DEG_to_RAD(x) M_PI*(x)/(180.0)
#define RAD_to_DEG(x) (x*180.0)/M_PI
#define MIN_to_DEG(x) x/60.0
#define in( l1, x, l2 ) l1 <= x && x <= l2 ? true : false
#define EQUAL(a,b) fabs(a-b) < EPS

int LSB( int x )
{
	return x & (-x);
}

int MSB( int x )
{
	for (int i = 0; i < 4; ++i)
		x = x | ( x>>(1<<i) );
	return (x+1)>>1;
}

bool IsPow2( int x )
{
	return !(x & (x-1)) && x;
}

bool CheckBit( int x , int p )
{
	return (x & 1<<p);
}

int MaskFull( int n )
{
	return (1<<n)-1;
}

int SetBit( int n, int p )
{
	return n | ( 1<<p );
}

int ClearBit( int n, int p )
{
	return n & ( ~( 1<<p ) );
}

int InvertBit( int n , int p )
{
	return n ^ ( 1<<p );
}

int CountOnes( int n )
{
	int count = 0;
	while( n )
	{
		n = n&(n-1);
		++count;
	}
	return count;
}

void Subsets( vi &A , int N )
{
	for (int i = 0; i < 1<<N; ++i)
	{
		for (int j = 0; j < N ; ++j )
			if( (i & (1<<j)) )
				printf("%d ", A[j] );
		printf("\n");
	}
}

int main(int argc, char const *argv[])
{
	vi A;
	for (int i = 0; i < 3 ; ++i)
		A.pb( i+1 );

	Subsets( A , 3 );

	return 0;
}