#include <bits/stdc++.h>
using namespace std;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<ii> vii;
typedef vector<vii> vvii;
typedef long long ll;
typedef pair<ll,ll> llll;

#define pb push_back
#define fi first
#define se second
#define DEBUG if(1)
#define VISITED 2
#define EXPLORED 1
#define UNVISITED 0
#define pb push_back
#define EPS 1e-9
#define INF 1<<20
#define inf 0x3f3f3f3f
#define DEG_to_RAD(x) M_PI*(x)/(180.0)
#define RAD_to_DEG(x) (x*180.0)/M_PI
#define MIN_to_DEG(x) x/60.0
#define in( l1, x, l2 ) l1 <= x && x <= l2 ? true : false
#define EQUAL(a,b) fabs(a-b) < EPS
#define all(x) (x).begin() , (x).end()

vi depth;
vi parent;
vi low;

vvi Adj;

void init( int v )
{
	Adj.clear();
	Adj.resize(v);
	depth.clear();
	depth.resize(v,-1);
	parent.clear();
	parent.resize(v,-1);
	low.clear();
	low.resize(v);
}

void findArt( int v , int & d )
{
	depth[v] = low[v] = d++;
	for( auto u : Adj[v] )
	{
		if( depth[u] == -1 ) // vstd
		{
			parent[u] = v;
			findArt( u , d );
			if( low[u] >= depth[v] )
			{
				if( depth[v] != low[v] ) // Condição de Vertice
					printf("Vertice de articulação %d\n", v+1);
				if( low[u] > depth[v] ) // Condição de Ponte
					printf("Ponte de articulação entre %d %d\n", v+1, u+1);
			}
			low[v] = min( low[v] , low[u] );
		}
		else
		{
			if( parent[v] != u )
				low[v] = min( low[v] , depth[u] );
		}
	}
}

int main(int argc, char const *argv[])
{
	int v;
	int e;
	cin >> v >> e;
	init(v);
	int id = 0;
	string f, a;
	unordered_map<string,int> citys;

	for (int i = 0; i < e ; ++i)
	{
		cin >> f >> a;
		if( !citys.count(f) )
			citys[f] = id++;
		if( !citys.count(a) )
			citys[a] = id++;
		Adj[citys[f]].pb(citys[a]);
	}
	int q = 0;
	findArt(0,q);
	return 0;

}