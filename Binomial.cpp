#include <bits/stdc++.h>
using namespace std;
#define EPS 1e-9
#define INF INT_MAX/10
#define DEG_to_RAD(x) (x/180.0)*M_PI
#define RAD_to_DEG(x) (x*180.0)/M_PI
#define DEBUG if(0)
#define DEBUG2 if(0)
#define MIN_to_DEG(x) x/60.0
typedef long long ll;
const int MAX = 1000;

unordered_map< ll, unordered_map< ll, ll > >BC;

/* 
//     Coeficiente Binomial 				Propriedade
// 		( n )		n!				( n )		( n )	   (n+1)
//		( - )	=  ----				( - )	+ 	( - )	=  ( - )
//		( k )	  k!(n-k)!			( k )	  	(k-1)	   ( k )
// 		A soma da n linha é 2^n;
*/

ll binomial( ll n, ll k )
{
	if( k == 0LL || k == n ) 
		return 1LL;
	else if( k == 1LL || k == (n-1LL) ) 
		return n;
	else
	{
		if( BC[n][k] ) 
			return BC[n][k];
		return BC[n][k] = BC[n][n-k] = binomial(n-1, k-1) + binomial(n-1, k);
	}
}
int main()
{
	ll n;
	cin >> n;
	for ( ll i = 0; i <= n ; ++i)
	{
		for ( ll j = 0; j <= i; ++j )
		{
			if( j != 0LL ) printf(" ");
			cout << binomial( i, j );
		}
		printf("\n");
	}
	return 0;
}