#include <bits/stdc++.h>
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef vector< ii > vii;
#define pb push_back
unordered_map <string,int> idx_people;
vector<string> name_people;

vvi AdjList;
vi indegree;
vector<bool> removed;
vi top_order;

bool topologic_sort()
{
	queue<int> q;
	int u;
	for (int i = 0; i < AdjList.size() ; ++i)
		if( indegree[i] == 0)
			q.push(i);
	while( !q.empty() )
	{
		u = q.front(); q.pop();
		top_order.pb(u);
		removed[u] = true;
		for (int i = 0; i < AdjList[u].size() ; ++i)
		{
			int v = AdjList[u][i];
			if( !removed[v] && --indegree[v] == 0 )
				q.push(v);
		}
	}
	return AdjList.size() == top_order.size();
}

void all_clear( int n )
{
	name_people.clear();
	idx_people.clear();
	AdjList.clear();
	AdjList.resize(n);
	indegree.clear();
	indegree.resize(n,0);
	removed.clear();
	removed.resize(n,false);
	top_order.clear();
}

int main()
{
	int people, relation;
	int TC = 0;
	string name1, name2;
	while( scanf("%d\n", &people), people )
	{
		if(TC) printf("\n");
		all_clear( people );
		for (int i = 0; i < people; ++i)
		{
			cin >> name1;
			idx_people[name1] = i;
			name_people.pb( name1 );
		}
		for (int i = 0; i < people ; ++i)
		{
			cin >> name1 >> relation;
			for (int j = 0; j < relation; ++j)
			{
				cin >> name2;
				++indegree[ idx_people[name1] ];
				AdjList[ idx_people[name2] ].push_back( idx_people[name1] );
			}
		}
		printf("Teste %d\n", ++TC);
		if( topologic_sort() )
		{
			for (int i = 0; i < people ; ++i)
			{
				if(i) printf(" ");
				cout << name_people[ top_order[i] ];
			}
			printf("\n");
		}
		else
			printf("impossivel\n");
	}
	return 0;
}