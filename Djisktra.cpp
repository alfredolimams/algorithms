#include <bits/stdc++.h>
using namespace std;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<ii> vii;
typedef vector<vii> vvii;
#define pb push_back
#define fi first
#define se second
#define INF 0xf3f3f3
#define DEBUG if(0)
vvii AdjList;
void init(int vertex)
{
	AdjList.clear();
	AdjList.resize(vertex);
}
vi dijkstra( int vertex, int from )
{
	vi dist(vertex,INF);
	dist[from] = 0;
	priority_queue< ii, vector<ii>, greater<ii> > pq; pq.push( {0, from} );
	while( !pq.empty() )
	{
		ii front = pq.top(); pq.pop();
		int d = front.first, u = front.second;
		if (d > dist[u]) continue;
		for( auto p : AdjList[u] )
		{
			ii v = p;
			if( dist[u] + v.se < dist[v.fi] )
			{
				dist[v.fi] = dist[u] + v.se;
				pq.push( { dist[v.fi], v.fi } );
			}
		}
	}
	return dist;
}
int main()
{
	return 0;
}