#include <bits/stdc++.h>
using namespace std;
#define EPS 1e-9
#define INF 1<<20
#define inf 0x3f3f3f3f
#define pb push_back
#define DEG_to_RAD(x) (x/180.0)*M_PI
#define RAD_to_DEG(x) (x*180.0)/M_PI
#define fi first
#define se second
#define DEBUG if(0)
#define MIN_to_DEG(x) x/60.0
#define in( l1, x, l2 ) l1 <= x && x <= l2 ? true : false
#define EQUAL(a,b) fabs(a-b) < EPS
#define MAX 100
typedef vector<int> vi;
typedef pair<int,int> ii;
typedef vector<ii> vii;
typedef long long ll;
typedef vector<vi> vvi;

int main()
{
	int N;
	scanf("%d", &N);
	int A[N*N];
	for (int i = 0; i < N*N; ++i)
		scanf("%d", A+i );
	int up = 0, down = N-1, left = 0, right = N-1;
	int M[N][N];
	int idx = 0;
	while( idx < N*N )
	{
		// Esquerda
		for (int i = up ; i <= down ; ++i)
			M[i][left] = A[idx++];
		left++;
		// Baixo
		for (int i = left; i <= right; ++i)
			M[down][i] = A[idx++];
		down--;
		// Direita
		for (int i = down; i > up ; --i)
			M[i][right] = A[idx++];
		right--;
		// Cima
		for (int i = right+1; i >= left ; --i)
			M[up][i] = A[idx++];
		up++;
	}
	for (int i = 0; i < N ; ++i)
		for (int j = 0; j < N ; ++j)
			printf("%d\n", M[i][j] );
	return 0;
}