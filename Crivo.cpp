#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define INF 0x3f3f3f
#define fi first
#define se second
#define MAXC (int)1e6 + 10
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vii> vvii;
typedef tuple<int,int,int> iii;
typedef long long ll;

bitset<MAXC>prim;
vector<ll> primes;

void allprimes()
{
	prim[0] = prim[1] = true;
	ll limit = sqrt(MAXC);
	for (ll i = 2; i < limit; ++i)
	{
		if( !prim[i] )
		{
			primes.pb(i);
			for (ll j = 2; j*i < MAXC ; ++j)
				prim[j*i] = true;
		}
	}
	for (ll i = limit ; i <= MAXC; ++i)
		if( !prim[i] )
			primes.pb(i);
}
bool isPrime( ll number )
{
	return !prim[number];
}

int main()
{
	allprimes();
	ll limit = 104729;
	int idx = 0;
	cin >> idx;
	for (int i = 0; i < idx; ++i)
	{
		cout << primes[i] << endl;
	}
	return 0;
} 