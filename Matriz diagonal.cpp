#include <bits/stdc++.h>
using namespace std;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<ii> vii;
typedef vector<vii> vvii;
typedef long long ll;
typedef pair<ll,ll> llll;

#define pb push_back
#define fi first
#define se second
#define DEBUG if(0)
#define VISITED 2
#define EXPLORED 1
#define UNVISITED 0
#define pb push_back
#define EPS 1e-9
#define INF 1<<20
#define inf 0x3f3f3f3f
#define DEG_to_RAD(x) (x/180.0)*M_PI
#define RAD_to_DEG(x) (x*180.0)/M_PI
#define MIN_to_DEG(x) x/60.0
#define in( l1, x, l2 ) l1 <= x && x <= l2 ? true : false
#define EQUAL(a,b) fabs(a-b) < EPS

int main(int argc, char const *argv[])
{
	int L, C;
	cin >> L >> C;
	int M[L][C];
	int idx = 0;
	for (int i = 0 , j; i < L+C; ++i)
	{
		j = min( i , L-1 );
		for (int k = max( 0 , i - L + 1 ), l = j ; k < min( i+1 , C ) ; ++k , --l )
		{
			M[l][k] = ++idx;
		}
	}
	for (int i = 0; i < L ; ++i)
	{
		for (int j = 0; j < C ; ++j )
		{
			cout << M[i][j] << " ";
		}
		cout << endl;
	}
	return 0;
}