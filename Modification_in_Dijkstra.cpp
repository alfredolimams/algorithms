// Modification in Dijkstra 
#include <bits/stdc++.h>
using namespace std;

template<class T>
class PriorityQueue
{
	vector<T> heap;
	map<int,int> index;
	set<int> enqueue;
	int lChild(int idx)	{ return idx<<1; }
	int rChild(int idx)	{ return (idx<<1)|1; }
	int father(int idx)	{ return idx>>1;	}
	void upper(int idx)
	{
		while( idx > 1 && cmp(heap[idx],heap[father(idx)]) )
		{
			swap(heap[idx], heap[father(idx)]);
			swap(index[idx],index[father(idx)]);
			idx = father(idx);
		}
	}
	void lower(int idx)
	{
		while( lChild(idx) <= size() )
		{
			int child = lChild(idx);
			if( rChild(idx) <= size() && cmp(heap[rChild(idx)],heap[child]) )
				child = rChild(idx);
			if( cmp(heap[idx],heap[child]) )
				idx = size();
			else
			{
				swap(heap[idx],heap[child]);
				swap(index[idx],index[child]);
				idx = child;
			}
		}
	}
	bool cmp( T a, T b ) { return a.second < b.second;	}
public:
	PriorityQueue()	{ T nil; heap.push_back(nil);	}
	~PriorityQueue() { heap.clear(); }
	int size()	{ return heap.size()-1;  }
	bool empty() { return heap.size() <= 1;	}
	void add(T value)
	{
		if( enqueue.count(value.first) )
		{
			if( cmp(value,heap[index[value.first]]) )
			{
				heap[index[value.first]] = value;
				upper(index[value.first]);
			}
			else
			{
				heap[index[value.first]] = value;
				lower(index[value.first]);
			}
		}
		else
		{
			enqueue.insert(value.first);
			heap.push_back( value );
			index[value.first] = size(); 
			upper( size() );
		}
	}
	T minValue()
	{
		T temp = heap[1];
		swap( heap[1] , heap[size()] );
		swap( index[temp.first] , index[size()] );
		enqueue.erase(temp.first);
		heap.pop_back();
		lower(1);
		return temp;
	}
	void print()
	{
		cout << "Status: " << endl;
		for (int i = 1; i < heap.size() ; ++i)
		{
			printf("%7d%7d%7d\n", heap[i].first, heap[i].second, index[heap[i].first] );
		}
		cout << endl;
	}
};

class Grafo
{
	vector< vector< pair<int,int> > > AdjList;
	int qt_vertices;
	vector<int> weights;
	vector<int> steps;
public:
	Grafo(int vertices)
	{
		AdjList.resize(vertices);
		weights.resize(vertices);
		steps.resize(vertices);
		qt_vertices = vertices;
	}
	~Grafo()
	{
		AdjList.clear();
		weights.clear();
		steps.clear();
	}
	void addEdge( int from , int arrival, int weight = 0 )
	{
		if( from >= qt_vertices || arrival >= qt_vertices ) 
		{
			cout << "Inválido";
			return;
		}
		else
		{
			AdjList[from].push_back( {arrival,weight} );
		}
	}
	bool dijkstra(int from) 
	{
		if( from >= qt_vertices ) 
		{
			cout << "Inválido";
			return false;
		}
		for (int i = 0; i < qt_vertices ; ++i)
		{
			weights[i] = 1<<20;
			steps[i] = 1<<20;
		}
		weights[from] = steps[from] = 0;
		PriorityQueue< pair<int,int> > pq;
		pq.add( {from,0} ); 
		while ( !pq.empty() ) 
		{
			pair<int,int> ii = pq.minValue();
			for( auto u : AdjList[ii.first] )
			{
				if( weights[ii.first] + u.second < weights[u.first] )
				{
					weights[u.first] = weights[ii.first] + u.second;
					steps[u.first] = steps[ii.first] + 1;
					if( steps[u.first] >= qt_vertices ) return false;
					pq.add( {u.first,weights[u.first]} );	
				}
			}
		}
		return true;
	}
};

int main(int argc, char const *argv[])
{
	int qt = 7;
	PriorityQueue< pair<int,int> > pq;
	srand(time(NULL));
	//cin >> qt;
	vector< pair<int,int> > randows;
	pair<int,int> ii;
	for (int i = 1; i <= qt ; ++i)
	{
		ii = pair<int,int>(i,rand()%100);
		randows.push_back( ii );
		pq.add( ii );
		pq.print();
	}
	int updates;
	cin >> updates;
	for (int j = 0; j < updates ; ++j)
	{
		for (int i = 1; i <= qt; ++i)
		{
			int v = rand()%1000;
			cout << "Updando o " << i << " " << v << endl;
			pq.add( {i,v} );
			pq.print();
		}
	}

	return 0;
}