#include <bits/stdc++.h>
using namespace std;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<ii> vii;
typedef vector<vii> vvii;
typedef long long ll;
typedef pair<ll,ll> llll;

#define pb push_back
#define fi first
#define se second
#define DEBUG if(1)
#define VISITED 2
#define EXPLORED 1
#define UNVISITED 0
#define pb push_back
#define EPS 1e-9
#define INF 1<<20
#define inf 0x3f3f3f3f
#define DEG_to_RAD(x) M_PI*(x)/(180.0)
#define RAD_to_DEG(x) (x*180.0)/M_PI
#define MIN_to_DEG(x) x/60.0
#define in( l1, x, l2 ) l1 <= x && x <= l2 ? true : false
#define EQUAL(a,b) fabs(a-b) < EPS

struct point
{
    double x,y;
    point() { x = y = 0.0;  }
    point( double x_, double y_ ) : x(x_) , y(y_) {}
    bool operator < (point other) const
    {
        if( fabs(x-other.x) > EPS ) return x < other.x;
        return y < other.y;
    }
    bool operator == (point other) const
    {
        return (fabs(x - other.x) < EPS && (fabs(y - other.y) < EPS));
    }
    bool operator != (point other) const { return !(*this == other); }
    bool operator <= (point other) const { return *this < other || *this == other; }
    bool operator > (point other ) const { return !(*this<=other); }
    bool operator >= (point other ) const { return *this > other || *this == other; } 
    point operator + (point other)  { return point( x+other.x, y+other.y ); }
};
point pivot(0, 0);
// 0D - Points
double dist( point p1, point p2) { return hypot(p1.x - p2.x, p1.y - p2.y); }
point rotate( point p, double theta )
{
    if(theta == 90.0) return point( -p.y , p.x );
    else if(theta == -90.0) return point( p.y, -p.x);
    double rad = DEG_to_RAD(theta); 
    return point( p.x*cos(rad) - p.y*sin(rad) , p.x*sin(rad) + p.y*cos(rad) );
}
// 1D - Vectors and Lines
struct line { double a, b, c; };
void pointsToLine( point p1, point p2, line &l )
{
    if( fabs(p1.x - p2.x) < EPS ) { l.a = 1.0; l.b = 0.0; l.c =-p1.x; }
    else { l.a = -(double)(p1.y - p2.y) / (p1.x - p2.x);  l.b = 1.0; l.c = -(double)(l.a * p1.x) - p1.y; }
}
bool areParallel(line l1, line l2) { return (fabs(l1.a-l2.a) < EPS) && (fabs(l1.b-l2.b) < EPS); }
bool areSame(line l1, line l2) { return areParallel(l1 ,l2) && (fabs(l1.c - l2.c) < EPS); }
bool areIntersect(line l1, line l2, point &p)
{
    if (areParallel(l1, l2)) return false;
    p.x = (l2.b * l1.c - l1.b * l2.c) / (l2.a * l1.b - l1.a * l2.b);
    if (fabs(l1.b) > EPS) p.y = -(l1.a * p.x + l1.c);
    else p.y = -(l2.a * p.x + l2.c);
    return true; 
}
struct vec
{
    double x,y;
    vec(double _x, double _y) : x(_x), y(_y) {}
};
vec toVec(point a, point b) { return vec(b.x-a.x, b.y-a.y);  }
vec scale(vec v, double s) { return vec( v.x*s , v.y*s ); }
point translate(point p, vec v) { return point( p.x+v.x, p.y+v.y ); }
double dot(vec a, vec b) { return a.x*b.x + a.y*b.y; }
double norm_sq(vec v) { return dot(v,v); }
double distToLine( point p, point a, point b, point *c)
{
    vec ap = toVec(a,p), ab = toVec(a,b);
    double u = dot(ap,ab)/norm_sq(ab);
    *c = translate(a , scale(ab, u));
    return dist(p,*c);
}
double distToLineSegment(point p, point a, point b, point *c)
{
    vec ap = toVec(a, p), ab = toVec(a, b);
    double u = dot(ap, ab)/norm_sq(ab);
    if( u < 0.0 ) { *c = a; return dist(p,a); }
    if( u > 1.0 ) { *c = b; return dist(p,b); }
    return distToLine( p, a, b, c);
}
double angle( point a, point o, point b) 
{
    vec oa = toVec(o,a), ob = toVec(o,b);
    return acos( dot(oa,ob)/sqrt(norm_sq(oa)*norm_sq(ob)) );
}
double cross(vec a, vec b) { return a.x*b.y - a.y*b.x; }
bool ccw(point P0, point P1, point P2)
{
    return ( (P1.x - P0.x) * (P2.y - P0.y) - (P2.x -  P0.x) * (P1.y - P0.y) ) > 0.0;
} 
bool collinear(point p, point q, point r) { return fabs(cross(toVec(p, q), toVec(p, r))) < EPS; }
int insideCircle( point p, point c, double r) { double distpc = dist(p , c); return distpc < r ? 0 : distpc == r ? 1 : 2 ; }
bool circle2PtsRad( point p1, point p2, double r, point &c)
{
    double d2 = dist(p1,p2), det = (r*r)/(d2) - 0.25;
    if( det < 0.0 ) return false;
    double h = sqrt(det);
    c.x = (p1.x+p2.x)*0.5 + (p1.y-p2.y)*h;
    c.y = (p1.y+p2.y)*0.5 + (p2.x-p1.x)*h;
    return true;
}
// Triangulo 
double perimeter( double ab, double bc, double ac ) { return ab+bc+ac; }
double perimeter( point a, point b, point c ) { return perimeter( dist(a,b) , dist(b,c) , dist(a,c) );  }
double area( double ab, double bc, double ac ) { double s = perimeter(ab,bc, ac)/2.0; return sqrt(s*(s-ab)*(s-bc)*(s-ac)); }
double area( point a, point b, point c ) { return area( dist(a,b) , dist(b,c) , dist(a,c) ); }
// Circulo
double rInCircle(double ab, double bc, double ca) { return area(ab, bc, ca)/(0.5*perimeter(ab, bc, ca)); }
double rInCircle(point a, point b, point c) { return rInCircle(dist(a, b), dist(b, c), dist(c, a)); }
int inCircle(point p1, point p2, point p3, point &ctr, double &r)
{
    r = rInCircle(p1, p2, p3);
    if (fabs(r) < EPS) return 0;
    line l1, l2;
    double ratio = dist(p1, p2) / dist(p1, p3);
    point p = translate(p2, scale(toVec(p2, p3), ratio / (1 + ratio)));
    pointsToLine(p1, p, l1);
    ratio = dist(p2, p1) / dist(p2, p3);
    p = translate(p1, scale(toVec(p1, p3), ratio / (1 + ratio)));
    pointsToLine(p2, p, l2);
    areIntersect(l1, l2, ctr); 
    return 1;
}
double rCircumCircle(double ab, double bc, double ca) { return ab * bc * ca / (4.0 * area(ab, bc, ca)); }
double rCircumCircle(point a, point b, point c) { return rCircumCircle(dist(a, b), dist(b, c), dist(c, a)); }
bool isTriangle( double ab, double bc, double ac ) { return max(ab, max(bc,ac) ) < ab+bc+ac-max(ab, max(bc,ac) ); }
bool isTriangle( point p, point q, point r ) { return !collinear(p,q,r); }
// Lei do seno e cosseno
double law_cos_ang( double a, double b, double c) { return acos( (a*a+b*b-c*c)/(2*a*b) ); }
double law_cos (double a, double b, double ang) { return sqrt( a*a + b*b - 2*a*b*cos(DEG_to_RAD(ang))); }
double law_sin_ang(double a, double ang_a , double ang_b ) { return a*sin(DEG_to_RAD(ang_b))/sin(DEG_to_RAD(ang_a)); }
double law_sin( double a, double b, double ang_a ) { return asin(b*sin(DEG_to_RAD(ang_a))/a); }
// Poligonos
// Não se esquecer do loop back push_back( p[0] )
double perimeter(const vector<point> &P)
{
    double result = 0.0;
    for (int i = 0; i < (int)P.size()-1 ; ++i)
        result += dist(P[i], P[i+1]);
    return result;
}
double area( const vector<point> &P )
{
    double result = 0.0;
    for (int i = 0; i < (int)P.size()-1 ; ++i)
        result += (P[i].x*P[i+1].y - P[i+1].x*P[i].y);
    return 0.5*fabs(result);
}
bool isConvex( const vector<point> &P )
{
    int sz = (int)P.size(); if(sz <= 3) return false;
    bool isLeft = ccw(P[0],P[1],P[2]);
    for (int i = 1; i < sz - 1 ; ++i)
        if( ccw(P[i],P[i+1],P[(i+2)==sz ? 1 : i+2]) != isLeft )
            return false;
    return true;
}
bool inPolygon(point pt , const vector<point> &P )
{
    int    cn = 0;    // the  crossing number counter
 
    // loop through all edges of the polygon
    for (int i=0; i< P.size()-1 ; i++) {    // edge from V[i]  to V[i+1]
       if (((P[i].y <= pt.y) && (P[i+1].y > pt.y))     // an upward crossing
        || ((P[i].y > pt.y) && (P[i+1].y <=  pt.y))) { // a downward crossing
            // compute  the actual edge-ray intersect x-coordinate
            float vt = (float)(pt.y  - P[i].y) / (P[i+1].y - P[i].y);
            if (pt.x <  P[i].x + vt * (P[i+1].x - P[i].x)) // P.x < intersect
                 ++cn;   // a valid crossing of y=P.y right of P.x
        }
    }
    return (cn&1);    // 0 if even (out), and 1 if  odd (in)
}
point lineIntersectSeg(point p, point q, point A, point B) 
{
    double a = B.y - A.y;
    double b = A.x - B.x;
    double c = B.x * A.y - A.x * B.y;
    double u = fabs(a * p.x + b * p.y + c);
    double v = fabs(a * q.x + b * q.y + c);
    return point((p.x * v + q.x * u) / (u+v), (p.y * v + q.y * u) / (u+v));
}
vector<point> cutPolygon(point a, point b, const vector<point> &Q)
{
    vector<point> P;
    for (int i = 0; i < (int)Q.size(); i++)
    {
        double left1 = cross(toVec(a, b), toVec(a, Q[i])), left2 = 0;
        if (i != (int)Q.size()-1) left2 = cross(toVec(a, b), toVec(a, Q[i+1]));
        if (left1 > -EPS) P.push_back(Q[i]);
        if (left1 * left2 < -EPS) P.push_back(lineIntersectSeg(Q[i], Q[i+1], a, b));
    }
    if (!P.empty() && !(P.back() == P.front())) P.push_back(P.front());
    return P;
}
bool angleCmp(point a, point b)
{
    if (collinear(pivot, a, b)) return dist(pivot, a) < dist(pivot, b);
    double d1x = a.x - pivot.x, d1y = a.y - pivot.y;
    double d2x = b.x - pivot.x, d2y = b.y - pivot.y;
    return (atan2(d1y, d1x) - atan2(d2y, d2x)) < 0;
}
double cross(const Point &O, const Point &A, const Point &B)
{
	return (A.x - O.x) * (B.y - O.y) - (A.y - O.y) * (B.x - O.x);
}

vector<point> convex_hull(vector<point> P)
{
	int n = P.size(), k = 0;
	vector<point> H;
	sort(P.begin(), P.end());
	for (int i = 0; i < n; ++i) {
		while (k >= 2 && cross(H[k-2], H[k-1], P[i]) <= 0) k--, H.pop_back();
		H.push_back(P[i]); k++;
	}
	for (int i = n-2, t = k+1; i >= 0; i--) {
		while (k >= t && cross(H[k-2], H[k-1], P[i]) <= 0) k--, H.pop_back();
		H.push_back(P[i]); k++;
	}
	H.pop_back();
	return H;
}
int main()
{
    vector<point> Polygon;
    vector<point> Plants;
    int P, V;
    int y,x;
    int ans = 0;
    scanf("%d%d", &P, &V);
    for (int i = 0; i < P; ++i)
    {
        scanf("%d%d", &y, &x);
        Polygon.pb( point(y,x) );
    }
    Polygon.pb(Polygon[0]);
    for (int i = 0; i < V; ++i)
    {
        scanf("%d%d", &y, &x);
        if( inPolygon( point(y,x), Polygon ) )
            ans += i;
    }
    printf("%d\n", ans);
    return 0;
}