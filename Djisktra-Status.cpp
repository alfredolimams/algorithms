#include <bits/stdc++.h>
using namespace std;
#define EPS 1e-9
#define INF 1<<20
#define inf 0x3f3f3f3f
#define pb push_back
#define DEG_to_RAD(x) (x/180.0)*M_PI
#define RAD_to_DEG(x) (x*180.0)/M_PI
#define fi first
#define se second
#define DEBUG if(0)
#define MIN_to_DEG(x) x/60.0
#define in( l1, x, l2 ) l1 <= x && x <= l2 ? true : false
#define EQUAL(a,b) fabs(a-b) < EPS
#define MAX 100
typedef vector<int> vi;
typedef pair<int,int> ii;
typedef vector<ii> vii;
typedef long long ll;
typedef vector<vi> vvi;
vector< vector< pair<int, bool> > > AdjList;


void clear( int v )
{
	AdjList.clear();
	AdjList.resize(v);
}

int dijkstra( int f, int a, int v )
{
	vvi dist;
	dist.resize(3);
	for (int i = 0; i < 3; ++i)
		dist[i].resize( v, inf );
	dist[0][f] = 0;
	priority_queue< ii, vector<ii>, greater<ii> > pq; pq.push( {0, f} );
	while( !pq.empty() )
	{
		ii front = pq.top(); pq.pop();
		int d = front.fi, u = front.se;
		int status = d%3;
		if( d > dist[status][u] )
			continue;
		for ( auto s : AdjList[u] )
		{
			int fr = s.fi; bool t = s.se;
			DEBUG cout << "status = " << status << endl;
			DEBUG cout << "Aresta = " << u << " " << fr << " on: " << t << endl;
			DEBUG cout << "D = " << dist[status][u] << endl;
			if( t && status )
				continue;
			if( !t && !status )
				continue;
			if( d + 1 < dist[ (status+1)%3 ][fr] )
			{
				dist[ (status+1)%3 ][fr] = d + 1;
				pq.push( { d+1 , fr } );	
			}
		}
	}
	int d = inf;

	DEBUG
	for (int i = 0; i < 3 ; ++i)
	{
		for (int j = 0; j < v ; ++j )
		{
			printf("|%10d|", dist[i][j] );
		}
		printf("\n");	
	}
	for (int i = 0; i < 3 ; ++i)
		d = min( d, dist[i][a] );
	return d;
}
int main()
{
	int vertex, from, arrival, edges;
	int f, a, s;
	scanf("%d%d%d%d", &vertex, &from, &arrival, &edges);
	clear(vertex);
	for (int i = 0; i < edges ; ++i)
	{
		scanf("%d%d%d", &f, &a, &s);
		AdjList[f].pb( {a,s} );
	}
	int d = dijkstra(from, arrival, vertex);
	if( d == inf )
		printf("*\n");
	else printf("%d\n", d );
	return 0;
}