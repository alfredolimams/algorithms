#include <bits/stdc++.h>
using namespace std;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<ii> vii;
typedef vector<vii> vvii;
typedef long long ll;
typedef pair<ll,ll> llll;

#define pb push_back
#define fi first
#define se second
#define DEBUG if(1)
#define VISITED 2
#define EXPLORED 1
#define UNVISITED 0
#define pb push_back
#define EPS 1e-9
#define INF 1<<20
#define inf 0x3f3f3f3f
#define DEG_to_RAD(x) (x/180.0)*M_PI
#define RAD_to_DEG(x) (x*180.0)/M_PI
#define MIN_to_DEG(x) x/60.0
#define in( l1, x, l2 ) l1 <= x && x <= l2 ? true : false
#define EQUAL(a,b) fabs(a-b) < EPS

vi parent;

int root( int x )
{
	if( x == parent[x] )
		return x;
	return parent[x] = root( parent[x] );
}

void union_root(int x, int y)
{
	parent[ root(x) ] = root(y);
}

void initSet(int n)
{
	parent.clear();
	parent.resize(n);
	for (int i = 0; i < n; ++i)
		parent[i] = i;
}
bool sameSet( int i , int j )
{
	return root(i) == root(j);
}
int main(int argc, char const *argv[])
{
	return 0;
}