#include <bits/stdc++.h>
using namespace std;
#define EPS 1e-9
#define INF INT_MAX/10
#define DEG_to_RAD(x) (x/180.0)*M_PI
#define RAD_to_DEG(x) (x*180.0)/M_PI
#define DEBUG if(0)
#define DEBUG2 if(0)
#define MIN_to_DEG(x) x/60.0
typedef long long ll;
typedef vector<int> vi;

int gcd( int a, int b )
{
	if( !b ) 
		return a;
	return gcd( b, a%b );
}
int lmc (int a, int b )
{
	return a*b/gcd( a, b );
}
int dividers( int n )
{
	int i = 1;
	int ans = 0;
	while( i*i <= n )
	{
		if( n%i == 0 && i*i == n )
			ans += 2;
		else if( n%i == 0 )
			++ans;
	}
	return ans;
}


// Exponencial O ( log e )
int fexp( int b, int e )
{
	if( !e )
		return 1;
	else
	{
		int v = fexp( b , e>>1 );
		if( e&1 )
			return b * v;
		return v;
	}
}

// Exponencial O ( log e )
int fexp( int b , int e , int mod )
{
	if( !e )
		return 1;
	else
	{
		int v = fexp( b , e>>1 , mod )%mod;
		if( e&1 )
			return (v*b)%mod;
		return v;
	}
}

// Encontrar o inverso de a em um anel O ( log mod )
int invert( int a , int mod )
{
	return fexp( a, mod-2 );
}


int main()
{
	int a;
	scanf("%d", &a);
	printf("%d\n", dividers(a) );
	return 0;
}